package me.coweery.codegen

import com.google.auto.service.AutoService
import me.coweery.codegen.annotations.BridgePresenter
import me.coweery.codegen.annotations.BridgeView
import me.coweery.codegen.models.ViewProxyDescription
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.TypeElement
import javax.lang.model.util.Elements
import javax.tools.Diagnostic

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor::class)
class BridgeKapt : AbstractProcessor() {

    private lateinit var messager: Messager
    private lateinit var filer: Filer
    private lateinit var elementUtils: Elements
    private val viewProxyGenerator by lazy { ViewProxyGenerator(elementUtils) }
    private val bridgePresentersGenerator by lazy {
        BridgePresentersGenerator({
            messager.printMessage(Diagnostic.Kind.WARNING, it)

        }, elementUtils)
    }

    override fun init(p0: ProcessingEnvironment) {
        messager = p0.messager
        filer = p0.filer
        elementUtils = p0.elementUtils
        super.init(p0)
    }

    override fun process(p0: MutableSet<out TypeElement>, p1: RoundEnvironment): Boolean {
        messager.printMessage(Diagnostic.Kind.WARNING, "=====================\n\n\n")

        if (p0.isEmpty()) {
            return true
        }
        val views = p1.getElementsAnnotatedWith(BridgeView::class.java).map { it as TypeElement }
        val descriptions = processViews(views)

        val presenters = p1.getElementsAnnotatedWith(BridgePresenter::class.java).map { it as TypeElement }
        processPresenters(presenters, descriptions)
        return true
    }


    override fun getSupportedAnnotationTypes(): MutableSet<String> {

        return mutableSetOf(BridgeView::class.java.name, BridgePresenter::class.java.name)
    }

    private fun processViews(views: List<TypeElement>): List<ViewProxyDescription> {
        return views.map {
            val proxy = viewProxyGenerator.generateViewProxy(it)
            val file = filer.createSourceFile(proxy.packageName + "." + proxy.name)
            file.openWriter().apply {
                write(proxy.content)
                close()
            }
            proxy
        }
    }

    private fun processPresenters(presenters: List<TypeElement>, viewDescriptions: List<ViewProxyDescription>) {

        presenters.forEach {
            val presenter = bridgePresentersGenerator.generateBridgePresenter(it, viewDescriptions)
            messager.printMessage(Diagnostic.Kind.WARNING, "${presenter.name}\n\n\n")
            val file = filer.createSourceFile(presenter.packageName + "." + presenter.name)
            file.openWriter().apply {
                write(presenter.content)
                close()
            }
        }

    }
}