package me.coweery.codegen

interface MvpPresenter<T> {

    fun attachView(view: T)
}