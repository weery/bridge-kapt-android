package me.coweery.codegen.models

import com.fasterxml.jackson.databind.node.POJONode

interface Args {

    fun toPOJONode(): POJONode {
        return POJONode(this)
    }
}