package me.coweery.codegen.models

class ViewProxyDescription(

    val name: String,
    val packageName: String,
    val viewName: String,
    val content: String
)