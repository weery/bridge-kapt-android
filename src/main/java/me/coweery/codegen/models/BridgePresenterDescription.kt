package me.coweery.codegen.models

class BridgePresenterDescription(

    val name: String,
    val packageName: String,
    val content: String
)