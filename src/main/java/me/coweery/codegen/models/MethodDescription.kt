package me.coweery.codegen.models

class MethodDescription(
    val name: String,
    val params: List<String>,
    val paramTypes: List<String>
) {

    val paramsWithTypes by lazy {
        params.zip(paramTypes) { p, t ->
            Pair(p, t)
        }
    }
}