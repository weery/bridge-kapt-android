package me.coweery.codegen

import me.coweery.codegen.models.MethodDescription
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.util.Elements

interface BaseGenerator {

    fun getGeneratedObjectName(
        name: String,
        element: TypeElement,
        preffix: String = "",
        postfix: String = ""
    ): String {
        return if (element.enclosingElement.kind == ElementKind.INTERFACE || element.enclosingElement.kind == ElementKind.CLASS) {
            getGeneratedObjectName(
                element.enclosingElement.simpleName.toString() + name,
                element.enclosingElement as TypeElement,
                preffix,
                postfix
            )
        } else {
            preffix + name + postfix
        }
    }

    fun TypeElement.getFullClassName(): String = asType().toString()

    fun TypeElement.getClassName(elementUtils: Elements): String =
        asType().toString().replace(elementUtils.getPackageOf(this).toString() + ".", "")

    fun parseMethods(element: TypeElement): List<MethodDescription> {
        return element.enclosedElements
            .asSequence()
            .filter { it is ExecutableElement }
            .map { it as ExecutableElement }
            .map {
                if (it.returnType.toString() != "void") throw IllegalArgumentException()
                val params = it.parameters.map { it.simpleName.toString() }
                val types = it.parameters.map { it.asType().toString() }
                MethodDescription(it.simpleName.toString(), params, types)
            }
            .toList()
    }
}