package me.coweery.codegen

import me.coweery.codegen.annotations.BridgeView
import me.coweery.codegen.models.BridgePresenterDescription
import me.coweery.codegen.models.MethodDescription
import me.coweery.codegen.models.ViewProxyDescription
import javax.lang.model.element.TypeElement
import javax.lang.model.type.DeclaredType
import javax.lang.model.util.Elements

class BridgePresentersGenerator(
    private val logger: (String) -> Unit,
    private val elementsUtils: Elements
) : BaseGenerator {


    fun generateBridgePresenter(
        presenter: TypeElement,
        viewsDescriptions: List<ViewProxyDescription>
    ): BridgePresenterDescription {


        val fullViewProxyClassName = viewsDescriptions.firstOrNull {
            it.packageName + "." + it.viewName == getViewInterfaceName(presenter)
        } ?: throw IllegalArgumentException()

        val (overrided, imports) = getOverridedMethods(parseMethods(presenter))

        val content = buildBody(
            getPresenterPackage(presenter),
            getPresenterClassName(presenter),
            getPresenterInterfaceNamne(presenter),
            imports,
            getViewInterfaceName(presenter) ?: throw IllegalArgumentException(),
            fullViewProxyClassName.packageName + "." + fullViewProxyClassName.name,
            overrided
        )
        return BridgePresenterDescription(getPresenterClassName(presenter), getPresenterPackage(presenter), content)
    }

    fun getPresenterClassName(presenter: TypeElement): String {
        return getGeneratedObjectName(presenter.simpleName.toString(), presenter, "Bridge")
    }

    private fun getPresenterFullClassName(presenter: TypeElement): String {
        return getPresenterPackage(presenter) +
                "." +
                getGeneratedObjectName(presenter.simpleName.toString(), presenter, "Bridge")
    }

    private fun getPresenterPackage(presenter: TypeElement): String = elementsUtils.getPackageOf(presenter).toString()

    private fun getPresenterInterfaceNamne(presenter: TypeElement) = presenter.asType().toString().replace(
        getPresenterPackage(presenter) + ".",
        ""
    )

    private fun getViewInterfaceName(presenter: TypeElement): String? {

        return presenter.interfaces
            .asSequence()
            .map { (it as DeclaredType) }
            .firstOrNull {
                it.asElement().toString() == MvpPresenter::class.java.name
            }
            ?.let {
                it.typeArguments.firstOrNull {
                    it.getAnnotationsByType(BridgeView::class.java)?.toString() != null
                }?.toString()
            }
    }

    private fun getOverridedMethods(descriptions: List<MethodDescription>): Pair<String, Set<String>> {

        val imports = mutableSetOf<String>()
        if (descriptions.isEmpty()) return Pair("", emptySet())
        return descriptions.map {

            imports.addAll(
                it.paramTypes.map { "import $it;" }
            )

            val args = it.paramsWithTypes.fold("") { acc, pair -> "$acc, ${pair.second} ${pair.first} " }

            "@Override\n" +
                    "    public void ${it.name}(${getArgumentsString(it)}) {\n" +
                    "        bridge.execute(remoteObjectId, \"${it.name}\", " +
                    "objectMapper.valueToTree(new ${it.name}ArgsWrap(${it.params.reduceOrDefault("") { acc, s -> "$acc, $s" }})));\n" +
                    "    }" + "\n ${createArgsWrapClass(it)} \n"
        }
            .reduce { acc, s -> "$acc \n\n $s" }
            .let {
                Pair(it, imports)
            }
    }

    private fun buildBody(
        presenterPackage: String,
        presenterClassName: String,
        presenterInterfaceName: String,
        imports: Set<String>,
        viewInterfaceName: String,
        viewProxyFullClassName: String,
        overrided: String
    ): String {


        return "package $presenterPackage;\n" +
                "\n" +
                "import com.fasterxml.jackson.databind.ObjectMapper;\n" +
                "import me.coweery.bridge.Bridge;\n" +
                "import com.fasterxml.jackson.annotation.JsonProperty;\n" +
                "import me.coweery.codegen.viewProxy.ViewProxy;\n" +
                (if (imports.isNotEmpty()) imports.reduce { acc, s -> acc + s + "\n" } else "") +
                "\n" +
                "public class $presenterClassName implements $presenterInterfaceName {\n" +
                "\n" +
                "    private final Bridge bridge;\n" +
                "    private final String remoteObjectId;\n" +
                "    private final ObjectMapper objectMapper = new ObjectMapper();\n" +
                "\n" +
                "    public $presenterClassName(\n" +
                "            Bridge bridge\n" +
                "    ) {\n" +
                "        this.bridge = bridge;\n" +
                "        remoteObjectId = bridge.createPresenter($presenterInterfaceName.class.getName()).blockingGet();\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    @Override\n" +
                "    public void attachView($viewInterfaceName view) {\n" +
                "\n" +
                "        final ViewProxy<$viewInterfaceName> viewProxy = new $viewProxyFullClassName(view);\n" +
                "        bridge.attachView(remoteObjectId, viewProxy);\n" +
                "    }\n" +
                "\n" +
                overrided +
                "\n" +
                "}"
    }

    private fun createArgsWrapClass(methodDescription: MethodDescription): String {

        val head = "private class ${methodDescription.name}ArgsWrap {\n\n"

        val fields = methodDescription.paramsWithTypes.map {
            val (name, type) = it

            return@map "@JsonProperty(\"$name\")\n" +
                    "        private $type $name;"
        }
            .reduceOrDefault("") { acc, s ->
                "$acc \n $s \n"
            }

        val allArgsConstructorHead = "public ${methodDescription.name}ArgsWrap"
        val constructorParams = methodDescription.paramsWithTypes
            .map {
                val (name, type) = it
                return@map "$type $name"
            }
            .reduceOrDefault("") { acc, s -> "$acc, $s" }


        val constructorBody = "\n" + methodDescription.params.map {
            "this.$it = $it;\n"
        }.reduceOrDefault("") { acc, s -> "$acc $s" }

        val noArgsConstructor = if (constructorParams != "") {
            "public ${methodDescription.name}ArgsWrap(){} "
        } else {
            ""
        }

        return "$head$fields\n$allArgsConstructorHead($constructorParams){$constructorBody}\n$noArgsConstructor\n }"

    }

    private fun <T> List<T>.reduceOrDefault(default: T, operation: (T, T) -> (T)): T {
        return if (isEmpty()) {
            default
        } else {
            reduce(operation)
        }
    }

    private fun getArgumentsString(methodDescription: MethodDescription): String {
        return methodDescription.paramsWithTypes
            .map {
                "${it.second} ${it.first}"
            }
            .reduceOrDefault("") { acc, s ->
                "$acc, $s"
            }
    }
}