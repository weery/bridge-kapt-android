package me.coweery.codegen

import me.coweery.codegen.models.MethodDescription
import me.coweery.codegen.models.ViewProxyDescription
import javax.lang.model.element.TypeElement
import javax.lang.model.util.Elements

class ViewProxyGenerator(
    private val elementUtils: Elements
) : BaseGenerator {


    fun generateViewProxy(view: TypeElement): ViewProxyDescription {

        val descriptions = parseMethods(view)
        val (overritedMethods, imports) = getOverridedMethods(descriptions)
        val execute = buildExecuteMethod(getMethodCases(descriptions))

        return ViewProxyDescription(
            getProxyName(view),
            elementUtils.getPackageOf(view).toString(),
            getViewName(view),
            buildBody(
                execute, overritedMethods, imports, getViewName(view), getProxyName(view),
                getViewPackage(view), getFullViewName(view)
            )
        )
    }

    private fun getProxyName(view: TypeElement): String {
        return getGeneratedObjectName(view.simpleName.toString(), view, postfix = "Proxy")
    }

    private fun getFullViewName(view: TypeElement): String {
        return view.getFullClassName()
    }

    private fun getViewName(view: TypeElement): String {
        return view.getClassName(elementUtils)
    }

    private fun getViewPackage(view: TypeElement): String {
        return elementUtils.getPackageOf(view).toString()
    }

    private fun getOverridedMethods(descriptions: List<MethodDescription>): Pair<String, Set<String>> {

        val imports = mutableSetOf<String>()

        return descriptions.map {
            val argsDeclarating =
                it.paramsWithTypes.map { it.second + " " + it.first }.reduce { acc, s -> "$acc,$s" }
            val argsList = it.params.reduce { acc, s -> "$acc, $s" }
            imports.addAll(
                it.paramTypes.map { "import $it;" }
            )
            "@Override\n" +
                    "    public void ${it.name}($argsDeclarating) {\n" +
                    "        if (viewRef.get() != null){\n" +
                    "            viewRef.get().${it.name}($argsList);\n" +
                    "        }\n" +
                    "    }"
        }
            .fold("") { acc, s -> "$acc \n\n $s" }
            .let {
                Pair(it, imports)
            }
    }

    private fun getMethodCases(descriptions: List<MethodDescription>): String {

        return descriptions.map {
            "case \"${it.name}\" : {\n" +
                    "                ${it.name}(\n" +
                    it.paramsWithTypes.map {
                        "objectMapper.convertValue(args.get(\"${it.first}\"), ${it.second}.class)"
                    }.reduce { acc, s -> "$acc, \n $s" } +
                    "                );\n" +
                    "                break;\n" +
                    "            }"

        }
            .fold("") { acc, s -> "$acc \n $s" }
    }

    private fun buildExecuteMethod(cases: String): String {
        return "@Override\n" +
                "    public void execute(@NotNull String method, @NotNull JsonNode args) {\n" +
                "        switch (method){\n" +
                cases +
                "\n" +
                "\n" +
                "        }\n" +
                "    }"
    }

    private fun buildBody(
        execute: String,
        overrided: String,
        imports: Set<String>,
        viewName: String,
        proxyName: String,
        viewPackage: String,
        fullViewName: String
    ): String {

        return "package $viewPackage;\n" +
                "\n" +
                imports.fold("") { acc, s -> acc + s + "\n" } +
                "import $fullViewName; \n" +
                "\n" +
                "import com.fasterxml.jackson.databind.JsonNode;\n" +
                "import com.fasterxml.jackson.databind.ObjectMapper;\n" +
                "import me.coweery.codegen.viewProxy.ViewProxy;\n" +
                "import org.jetbrains.annotations.NotNull;\n" +
                "import java.lang.ref.WeakReference;" +
                "\n\n" +
                "public class $proxyName implements ViewProxy<$viewName>, $viewName {\n" +
                "\n" +
                "\n" +
                "    private WeakReference<$viewName> viewRef;\n" +
                "    private ObjectMapper objectMapper = new ObjectMapper();\n" +
                "\n" +
                "    public $proxyName($viewName view) {\n" +
                "        viewRef = new WeakReference<>(view);\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    @NotNull\n" +
                "    @Override\n" +
                "    public String getId() {\n" +
                "        return Integer.toString(hashCode());\n" +
                "    }\n" +
                "\n" +
                "    @NotNull\n" +
                "    @Override\n" +
                "    public WeakReference<$viewName> getView() {\n" +
                "        return viewRef;\n" +
                "    }\n" +
                "\n" +
                execute +
                "\n" +
                overrided +
                "}\n"

    }

}