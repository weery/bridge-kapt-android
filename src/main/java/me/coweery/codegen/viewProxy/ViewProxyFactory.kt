package me.coweery.codegen.viewProxy

interface ViewProxyFactory<T> {

    fun createViewProxy(view: T): ViewProxy<T>
}