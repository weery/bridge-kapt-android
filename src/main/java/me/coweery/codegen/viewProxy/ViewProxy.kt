package me.coweery.codegen.viewProxy

import com.fasterxml.jackson.databind.JsonNode
import java.lang.ref.WeakReference

interface ViewProxy<T> {

    fun execute(method: String, args: JsonNode)

    val id: String

    val view: WeakReference<T>
}